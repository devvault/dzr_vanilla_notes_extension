modded class NoteMenu
{
	override Widget Init()
	{
		//Print("[dzr_vanilla_notes_extension] ::: Widget Init");
		//GetGame().Chat("[dzr_vanilla_notes_extension] ::: Widget Init", "colorAction");
		layoutRoot = GetGame().GetWorkspace().CreateWidgets("dzr_vanilla_notes_extension/gui/layout/dzr_inventory_note.layout");
		m_edit = MultilineEditBoxWidget.Cast( layoutRoot.FindAnyWidget("dzrTextInput") );
		m_html = HtmlWidget.Cast( layoutRoot.FindAnyWidget("dzrTextView") );
		m_confirm_button = ButtonWidget.Cast( layoutRoot.FindAnyWidgetById(IDC_OK) );
		
		return layoutRoot;
	}
}
